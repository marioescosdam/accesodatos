package com.marioea.jugueteriacomboboxxml;

public class Juguetes {
    //declaramos las variables que vamos a utilizar
    String marcaJuguete;
    String tipoJuguete;
   String rangoPrecio;
    String seccion;
    String calidadJuguete;
    /**
     * @author Mario Escos
     * @since 1.5
     * @version 1.1
     */
    //generamos constructor
    public Juguetes(String marcaJuguete, String tipoJuguete, String rangoPrecio, String seccion, String calidadJuguete) {
        this.marcaJuguete = marcaJuguete;
        this.tipoJuguete = tipoJuguete;
        this.rangoPrecio = rangoPrecio;
        this.seccion = seccion;
        this.calidadJuguete = calidadJuguete;
    }
    //generamos los getters y setters
    /**
     *
     * @return Marca del juguete
     */
    public String getMarcaJuguete() {
        return marcaJuguete;
    }
    /**
     *
     * @param marcaJuguete marca del juguete
     */
    public void setMarcaJuguete(String marcaJuguete) {
        this.marcaJuguete = marcaJuguete;
    }
    /**
     *
     * @return Tipo de Juguete
     */
    public String getTipoJuguete() {
        return tipoJuguete;
    }
    /**
     *
     * @param tipoJuguete Tipo de juguete
     */
    public void setTipoJuguete(String tipoJuguete) {
        this.tipoJuguete = tipoJuguete;
    }
    /**
     *
     * @return Tipo de juguete
     */
    public String getRangoPrecio() {
        return rangoPrecio;
    }
    /**
     *
     * @param rangoPrecio Rango de precio
     */
    public void setRangoPrecio(String rangoPrecio) {
        this.rangoPrecio = rangoPrecio;
    }
    /**
     *
     * @return Seccion
     */
    public String getSeccion() {
        return seccion;
    }
    /**
     *
     * @param seccion Seccion
     */
    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }
    /**
     *
     * @return CalidadJuguete
     */
    public String getCalidadJuguete() {
        return calidadJuguete;
    }
    /**
     *
     * @param calidadJuguete Calidad del Juguete
     */
    public void setCalidadJuguete(String calidadJuguete) {
        this.calidadJuguete = calidadJuguete;
    }
    //generamos el metodo to String

    @Override
    public String toString() {
        return "Juguetes{" +
                "marcaJuguete='" + marcaJuguete + '\'' +
                ", tipoJuguete='" + tipoJuguete + '\'' +
                ", rangoPrecio='" + rangoPrecio + '\'' +
                ", seccion='" + seccion + '\'' +
                ", calidadJuguete='" + calidadJuguete + '\'' +
                '}';
    }
}
