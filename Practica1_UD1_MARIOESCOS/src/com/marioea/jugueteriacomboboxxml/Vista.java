package com.marioea.jugueteriacomboboxxml;

import  org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class Vista {
    /**
     * @author Mario Escos
     * @since 1.5
     * @version 1.1
     */
    //elementos creados en la parte grafica de GUI
    private JFrame frame;
    private JPanel panel1;
    private JLabel lblMarcaJuguete;
    private JTextField marcaTxt;
    private JLabel lblTipoJuguete;
    private JTextField tipoTxt;
    private JTextField rangoprecioTxt;
    private JLabel lblRangoJuguete;
    private JComboBox comboBox;
    private JLabel lblSeccion;
    private JLabel lblCalidad;
    private JTextField calidadTxt;
    private JButton altaJugueteBtn;
    private JButton mostrarJugueteBtn;
    private JLabel lblJuguete;
    private JTextField seleccionTxt;
    //elementos introducidor por mi cuenta LinkedList y DefaultComboBoxModel
    private LinkedList<Juguetes> lista;
    private DefaultComboBoxModel<Juguetes> dcbm;
    String seccion="";
    //creamos contructor de Vista
    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        //añadimos un crear menu que lo utilizaremos mas abajo
        crearMenu();
        //añadimos setLocationRelativeTo para que la ventana aparezca en medio de nuestra pantalla
        frame.setLocationRelativeTo(null);

        lista = new LinkedList<>();
        dcbm = new DefaultComboBoxModel<>();
        comboBox.setModel(dcbm);

        /**
         * Alta de un juguete
         * @return
         */
        altaJugueteBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //dar de alta juguete
                altaJuguete(marcaTxt.getText(),tipoTxt.getText(),rangoprecioTxt.getText(),seleccionTxt.getText(),calidadTxt.getText());
                //listar juguetes de la lista del combo
                refrescarComboBox();
            }
        });
            /**
             * Mostrar  un juguete
             * @return
             */
            mostrarJugueteBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //muestro el juguete seleccionado
                Juguetes seleccionado= (Juguetes) dcbm.getSelectedItem();
                lblJuguete.setText(seleccionado.toString());
            }
        });

    }
    /**
     * Refrescar ComboBox
     * @return
     */
    //esto sirve para vaciar todos los labels y texts
    private void refrescarComboBox() {
        dcbm.removeAllElements();
        for (Juguetes juegos : lista) {
            dcbm.addElement(juegos);
        }
    }


    public static void main(String[] args) {
        //instanciamos la clase Vista para poder ejecutar la aplicacion
        Vista vista = new Vista();
    }
    //creamos el metodo para dar de alta un juguete
    private void altaJuguete(String marcaJuguete, String tipoJuguete, String rangoPrecio, String seccion, String calidadJuguete){
        lista.add(new Juguetes(marcaJuguete,tipoJuguete,rangoPrecio,seccion,calidadJuguete));
    }
    /**
     * Crear menu
     * @return
     */
    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        JMenuItem itemExportarXML = new JMenuItem("Exportar XML");
        JMenuItem itemImportarXML = new JMenuItem("Importar XML");


        barra.add(menu);
        /**
         * Exportar un juguete
         * @return
         */
        itemExportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
                if (opcionSeleccionada == JFileChooser.APPROVE_OPTION) {
                    File fichero = selectorArchivo.getSelectedFile();
                    exportarXML(fichero);
                }
            }
        });
        /**
         * Importar un juguete
         * @return
         */
        itemImportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcion = selectorArchivo.showOpenDialog(null);
                if (opcion == JFileChooser.APPROVE_OPTION) {
                    File fichero = selectorArchivo.getSelectedFile();
                    importarXML(fichero);
                    refrescarComboBox();
                }
            }
        });
                menu.add(itemExportarXML);
                menu.add(itemImportarXML);
                frame.setJMenuBar(barra);
    }
            private void importarXML(File fichero) {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                try {
                    DocumentBuilder builder = factory.newDocumentBuilder();
                    Document documento = builder.parse(fichero);

                    //Recorro cada uno de los nodos coche para obtener sus campos
                    NodeList juguetes = documento.getElementsByTagName("juguete");
                    for (int i = 0; i < juguetes.getLength(); i++) {
                        Node juguete = juguetes.item(i);
                        Element elemento = (Element) juguete;

                        //Obtengo los campos marca y modelo
                        String marcaJuguete = elemento.getElementsByTagName("marcaJuguete").item(0).getChildNodes().item(0).getNodeValue();
                        String tipoJuguete = elemento.getElementsByTagName("tipoJuguete").item(0).getChildNodes().item(0).getNodeValue();
                        String rangoPrecio = elemento.getElementsByTagName("rangoPrecio").item(0).getChildNodes().item(0).getNodeValue();
                        String seccion = elemento.getElementsByTagName("seccion").item(0).getChildNodes().item(0).getNodeValue();
                        String calidadJuguete = elemento.getElementsByTagName("calidadJuguete").item(0).getChildNodes().item(0).getNodeValue();

                        altaJuguete(marcaJuguete, tipoJuguete,rangoPrecio,seccion,calidadJuguete);
                    }
                } catch (ParserConfigurationException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (SAXException e) {
                    e.printStackTrace();
                }
            }
            private void exportarXML(File fichero) {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = null;

                try {
                    builder = factory.newDocumentBuilder();
                    DOMImplementation dom = builder.getDOMImplementation();

                    //Creo documento que representa arbol XML
                    Document documento = dom.createDocument(null, "xml", null);

                    //Creo el nodo raiz (juguetes) y lo añado al documento
                    Element raiz = documento.createElement("juguetes");
                    documento.getDocumentElement().appendChild(raiz);

                    Element nodoJuguete;
                    Element nodoDatos;
                    Text dato;

                    //Por cada juguete de la lista, creo un nodo juguete
                    for (Juguetes juguete : lista) {

                        //Creo un nodo juguete y lo añado al nodo raiz (juguetes)
                        nodoJuguete = documento.createElement("juguete");
                        raiz.appendChild(nodoJuguete);

                        //A cada nodo coche le añado los nodos marcaJuguete, tipoJuguete,rangoPrecio,seccion y calidadJuguete
                        nodoDatos = documento.createElement("marcaJuguete");
                        nodoJuguete.appendChild(nodoDatos);

                        //A cada nodo de datos le añado el dato
                        dato = documento.createTextNode(juguete.getMarcaJuguete());
                        nodoDatos.appendChild(dato);

                        nodoDatos = documento.createElement("tipoJuguete");
                        nodoJuguete.appendChild(nodoDatos);

                        dato = documento.createTextNode(juguete.getTipoJuguete());
                        nodoDatos.appendChild(dato);



                        nodoDatos = documento.createElement("rangoPrecio");
                        nodoJuguete.appendChild(nodoDatos);

                        dato = documento.createTextNode(juguete.getRangoPrecio());
                        nodoDatos.appendChild(dato);


                        nodoDatos = documento.createElement("seccion");
                        nodoJuguete.appendChild(nodoDatos);

                        dato = documento.createTextNode(juguete.getSeccion());
                        nodoDatos.appendChild(dato);

                        nodoDatos = documento.createElement("calidadJuguete");
                        nodoJuguete.appendChild(nodoDatos);

                        dato = documento.createTextNode(juguete.getCalidadJuguete());
                        nodoDatos.appendChild(dato);
                    }

                    //Transformo el documento anterior en un ficho de texto plano
                    Source src = new DOMSource(documento);
                    Result result = new StreamResult(fichero);

                    Transformer transformer = null;
                    transformer = TransformerFactory.newInstance().newTransformer();
                    transformer.transform(src, result);

                } catch (TransformerConfigurationException e) {
                    e.printStackTrace();
                } catch (TransformerException e) {
                    e.printStackTrace();
                } catch (ParserConfigurationException e) {
                    e.printStackTrace();
                }
            }
        }

